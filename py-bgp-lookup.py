#!/usr/bin/env python
'''
--------
This file is part of py-bgp-lookup.py.

 py-bgp-lookup.py is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 py-bgp-lookup.py is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with py-bgp-lookup.py.  If not, see <http://www.gnu.org/licenses/>.
--------

py-bgp-lookup.py:
--------
This is a tool that has been written to write out the results IP to BGP AS
numbers to a text file in JSON format. The input needs to be in the following
format:
1.1.1.1
1.2.1.1
1.2.2.1
1.2.2.2
1.2.2.3
1.2.3.3
1.3.3.3

--------
'''

from multiprocessing import Pool, Manager
from dns import resolver
from netaddr import IPNetwork
import sys
import json
import collections
import socket
import time

IGNORED = [
    '107.23.34.239',
    '127.0.0.1'
]


def save(name, dictionary):
    '''
    write out a file to disk
    '''
    filename = name + '.' + str(time.time()) + '.results'
    fileoutput = open(filename, 'w+')

    data = json.dumps(dict(dictionary),
                      collections.OrderedDict(),
                      sort_keys=True,
                      indent=4)

    print('Written results as : %s') % (filename)
    fileoutput.write(data)
    fileoutput.close()


def processor(networks):
    '''
    procesing dictionary into meaningful data
    '''
    begin = time.time()
    uniq = dict()
    for prefix in networks.keys():
        for asn in networks[prefix]:

            if not uniq.get(asn):
                uniq[asn] = 1
            else:
                uniq[asn] += 1

    end = time.time()
    print('time taken for processor() : %s') % (end - begin)
    return uniq


def convert(cidr):
    '''
    strip ip address back to /24
    '''
    _ip = IPNetwork(cidr)
    raw = list(_ip.supernet(24))
    network = str(raw[0]).split('/')[0]
    return network


def get_txt(as_origin, networks, country, _ip):
    '''
    using the subnet from swap lookup dns
    '''
    resolver.timeout = 3
    _rs = _ip.split('.')
    cymru_asn = '%s.%s.%s.%s.origin.asn.cymru.com' % (
        _rs[3], _rs[2], _rs[1], _rs[0])
    cymru_peer = '%s.%s.%s.%s.peer.asn.cymru.com' % (
        _rs[3], _rs[2], _rs[1], _rs[0])

    try:
        rrset = resolver.query(cymru_asn, 'TXT').rrset
    except resolver.NXDOMAIN:
        rrset = 'None \"Invalid | Invalid | Invalid | Invalid | Invalid \"'

    try:
        network_peers = resolver.query(cymru_peer, 'TXT').rrset
    except resolver.NXDOMAIN:
        network_peers = 'None \"Invalid | Invalid | Invalid | Invalid | Invalid \"'

    for peer in network_peers:
        spaces = str(peer).replace('"', '').split('|')
        prefix = spaces[1].strip()

        if not networks.get(prefix):
            networks[prefix] = list()
            networks[prefix] = spaces[0].strip().split(' ')

    for origin in rrset:
        orrset = str(origin).split('"')[1].split('|')
        _as = orrset[0].strip()
        _co = orrset[2].strip()

        if not as_origin.get(_as):
            as_origin[_as] = 1
        else:
            as_origin[_as] += 1

        if not country.get(_co):
            country[_co] = 1
        else:
            country[_co] += 1


def ipp():
    '''
    ip processing function
    '''
    start = time.time()
    manager = Manager()
    as_origin = manager.dict()
    country = manager.dict()
    networks = manager.dict()
    pool = Pool(processes=32)

    data = sys.stdin.readlines()
    print('Starting to lookup IP to BGP matching..')

    for (cidr) in data:
        try:
            socket.inet_aton(cidr.rstrip())
        except socket.gaierror:
            continue

        if cidr in IGNORED:
            continue
        else:
            _ip = convert(cidr)

        pool.apply_async(get_txt, [as_origin, networks, country, _ip])

    pool.close()
    pool.join()
    final = time.time()
    peer_list = processor(networks)
    save('origin_as', as_origin)
    save('origin_as_country', country)
    save('prefix_by_peer_as', networks)
    save('peer_as', peer_list)
    print('Total scan time : %s') % (final - start)


def main():
    '''
    call ipp()
    '''
    ipp()


if __name__ == "__main__":
    main()
