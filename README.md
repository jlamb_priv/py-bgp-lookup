## py-bgp-lookup

A simple script to take input from stdin (a list of ip addresses).

It will then conver the IP into a /24 network mask and look it up
against a known bgp dns query tool. The data is then output as a python
dict() for processing.

### Why DNS and not pygeoip
The tool uses DNS to query the cmyru asn tool instead of the pygeoip library.
Why? because the pygeoip data from various providers often lags behind the data
seen by the team cmyru DNS service. To have regularly updated data from the 
likes of maxmind a subscription is required.

#### References
- Maxmind Geolite : http://dev.maxmind.com/geoip/legacy/geolite/
- Maxmind : http://www.maxmind.com/en/geolocation_landing

### Notes:
This script uses multiprocess support in Python. The default is to launch 32 processes
to manage the DNS load concurrently. By doing 32 requests concurrently even very large
log files can be completed in a very small amount of time. However it puts significant
load on DNS infrastructure. In testing a 140MB list of IP addresses took around 1000 
seconds to process with a process pool of 32. However the same log took around 800 seconds
to process with 62 threads. The higher the threads the lower the rate of return because
of locking within the python master process. 
The biggest change you can make to increase the performance is to query a DNS server that 
has the objects already cached (unlikely) or query a very local DNS resolver (less than 10ms).

